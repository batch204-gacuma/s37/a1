const mongoose = required("mongoose");
const courseSchema = new mongoose.Schema({
	firstName:{
		type: String,
		required: [true, "first name is required"]
	},lastName:{
		type: String,
		required: [true, "last name is required"]
	}, email:{
		type: String,
		required: [true, "email is required"]
	}, password:{
		type: String,
		required:[true, "password is required"]
	}, isAdmin:{
		type: Boolean,
	}, mobileNo:{
		type: number
	}, enrollments:[
	{
		courseId:{
			type: String,
			required:[true, "UserID is required"]
		},enrolledOn:{
			type: Date,
			default: new Date()
		},status:{
			type: String,
			default: "Enrolled"
		}
	}

	]
})